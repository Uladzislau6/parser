<php

//спарсить 10 сайтов, вывести полученные сайты, 
/*отсортировав их по времени загрузки страницы в следующем виде:
 URL сайта 
    - Название сайта (title)
  - Кодировка 
  - Дата парсинга 
  - Время выполнения скрипта 
  - Ссылка на полученный файл сайта.
  




function pr($a){
    echo "<pre>";
    print_r($a);
    echo "</pre>";
}*/



$arr_sites = array (
    'https://yandex.by',
    'https://tut.by',
    'http://test.ru',
    'https://site.by',
    'https://onliner.by',
    'https://php.net',
    'https://www.twitch.tv',
    'https://www.youtube.com',
);

//pr($arr_sites);

$cookies = "cookie-".microtime(true).".txt";

$arr_sites_stat = array();

foreach($arr_sites as $key => $site){
    
    $start = microtime(1);
    
    $page = get_page('get', $site, array(), $cookies, $site );
    $content = $page['content'];
    $encoding = $page['encoding'];
    
    $title = getDataByOrder($content, '<title>', '</title>', 1);
    
    $site_name = str_replace(array('https://', 'http://'), array("", ""), $site);
    
    file_put_contents("temp/".$site_name.".html", $content);
    
    $finish = microtime(1);
    
    $time_parse = $finish - $start;
    
    $arr_sites_stat[] = array(
        'site' => $site,
        'site_name' => $site_name,
        'title' => $title,
        'time_parse' => $time_parse,
    );
    
    $arr_sites_stat['site'][$key] = $site;
    $arr_sites_stat['site_name'][$key] = $site_name;
    $arr_sites_stat['title'][$key] = $title;
    $arr_sites_stat['time_parse'][$key] = $time_parse;
    $arr_sites_stat['encoding'][$key] = $encoding;
  
    $arr_time_parse[$key] = $time_parse;
    
   // echo "$site - $site_name -  $title - $time_parse<br>";
}

//array_multisort($arr_site_stat['time_parse'], SORT_ASC);

pr($arr_time_parse);
asort($arr_time_parse);
pr($arr_sites_stat);

foreach($arr_time_parse as $key => $time_parse){
    
    $site = $arr_sites_stat['site'][$key];
    $site_name = $arr_sites_stat['site_name'][$key];
    $title = $arr_sites_stat['title'][$key];
    $encoding = $arr_sites_stat['encoding'][$key];
    
    echo "$key - $site - $site_name -  $title - $encoding - $time_parse<br>";
}



?>